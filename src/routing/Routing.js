import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import HomeScreen from '../components/HomeScreen/HomeScreen';

const Routing = () => {
  return (
    <BrowserRouter basename="/argentum-exp-tracker">
      <Switch>
        <Route path="/" exact>
          <HomeScreen />
        </Route>
      </Switch>
    </BrowserRouter>
  );
};

export default Routing;
