import React, { useState } from 'react';
import {
  AppBar,
  Button,
  Container,
  FormControl,
  IconButton,
  InputLabel,
  MenuItem,
  Select,
  styled,
  TextField,
  Toolbar,
  Typography,
} from '@material-ui/core';
import { Pets } from '@material-ui/icons';

const MainContainer = styled(Container)(({ theme }) => ({
  marginTop: theme.spacing(3),
  marginBottom: theme.spacing(13),
}));

const StyledInput = styled(TextField)(({ theme }) => ({
  marginBottom: theme.spacing(3),
}));

const CreaturesToKill = styled(({ ...others }) => <div {...others} />)(({ theme }) => ({
  // display: 'grid',
  // gridTemplateColumns: 'repeat(5, 1fr)',
  display: 'flex',
  flexWrap: 'wrap',
}));

const CREATURE_NAMES = {
  14000: 'Rinocerontes',
  6000: 'Sapos',
};

const HomeScreen = () => {
  const [currentExp, setCurrentExp] = useState();
  const [maxExp, setMaxExp] = useState();
  const [creature, setCreature] = useState();

  const [calculated, setCalculated] = useState(false);
  const [expPercentage, setExpPercentage] = useState(0);
  const [missingCreatures, setMissingCreatures] = useState(0);

  const calculateExp = () => {
    setCalculated(true);
    setExpPercentage(((currentExp * 100) / maxExp).toFixed(2));
    setMissingCreatures(Math.ceil((maxExp - currentExp) / creature));
  };

  const killCreature = () => {
    const newExp = currentExp + creature;
    setCurrentExp(newExp);
    setExpPercentage(((newExp * 100) / maxExp).toFixed(2));
    setMissingCreatures(Math.ceil((maxExp - newExp) / creature));
  };

  const reset = () => {
    setCalculated(false);
  };

  return (
    <>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" style={{ flexGrow: 1 }}>
            Argentum Exp Tracker
          </Typography>
        </Toolbar>
      </AppBar>
      {!calculated && (
        <MainContainer maxWidth="xs">
          <StyledInput
            required
            label="Exp. Actual"
            variant="outlined"
            fullWidth
            value={currentExp}
            onChange={(event) => setCurrentExp(parseInt(event.target.value))}
            type="number"
          />
          <StyledInput
            required
            label="Exp. Proximo Nivel"
            variant="outlined"
            fullWidth
            value={maxExp}
            onChange={(event) => setMaxExp(parseInt(event.target.value))}
            type="number"
          />
          <FormControl fullWidth variant="outlined">
            <InputLabel id="creature-label">Criatura</InputLabel>
            <Select
              labelId="creature-label"
              label="Criatura"
              value={creature}
              onChange={(event) => setCreature(event.target.value)}
            >
              <MenuItem value={14000}>Rinocerontes</MenuItem>
              <MenuItem value={6000}>Sapos</MenuItem>
            </Select>
          </FormControl>
          <Button
            variant="contained"
            fullWidth
            color="primary"
            style={{ marginTop: 24 }}
            onClick={calculateExp}
          >
            Calcular
          </Button>
        </MainContainer>
      )}
      {calculated && (
        <MainContainer maxWidth="xs">
          <Typography>
            Exp: {currentExp} / {maxExp} ({expPercentage}%)
          </Typography>
          <Typography>
            {CREATURE_NAMES[creature]}: {missingCreatures}
          </Typography>
          <CreaturesToKill>
            {Array(missingCreatures)
              .fill(0)
              .map(() => (
                <IconButton variant="contained" onClick={killCreature}>
                  <Pets />
                </IconButton>
              ))}
          </CreaturesToKill>
          <Button
            variant="contained"
            fullWidth
            color="primary"
            style={{ marginTop: 24 }}
            onClick={reset}
          >
            Editar
          </Button>
        </MainContainer>
      )}
    </>
  );
};

export default HomeScreen;
